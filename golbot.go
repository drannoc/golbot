package golbot

import (
	"errors"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/bwmarrin/discordgo"
	"github.com/monkeydioude/tools"
)

const (
	cacheDirPath = "/tmp/gb"
)

// Golbot defines the struct of the bot
type Golbot struct {
	session  *discordgo.Session
	commands map[string]Command
	cacheDir string
}

// New is used to set a good boi/t using default config
func New(token string) (*Golbot, error) {
	discord, err := discordgo.New(token)
	if err != nil {
		return nil, fmt.Errorf("[ERR ] %s", err)
	}

	g := &Golbot{
		commands: make(map[string]Command),
	}
	discord.AddHandler(g.messageHandler)
	g.session = discord

	return g, nil
}

func (g *Golbot) CreateCacheDir() error {
	return os.Mkdir(cacheDirPath, 0766)
}

// Run starts the bot
func (g *Golbot) Run() error {
	err := g.session.Open()
	if err != nil {
		return fmt.Errorf("[ERR ] %s", err)
	}

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
	g.session.Close()

	return errors.New("[INFO] Interrupt signal caught")
}

func (g *Golbot) messageHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Message.Content == "" || m.Author.ID == botID {
		return
	}

	if m.Message.Content == "/help" {
		g.SendMessage(&HelpMsg{
			cmds: &g.commands,
			m:    m,
		})
		return
	}

	for r, c := range g.commands {
		p, err := tools.MatchAndFind(r, m.Message.Content)

		if err != nil {
			continue
		}

		err = c.Do(m, p)
		if err != nil {
			log.Println(err)
		}
	}

}

func (g *Golbot) SendMessage(m Message) {
	m.Send(g.session)
}

func (g *Golbot) GetDiscordSession() *discordgo.Session {
	return g.session
}
