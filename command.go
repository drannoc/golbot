package golbot

import (
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
)

const (
	botID = "491595258929610757"
)

// Command is an interface defining how a command should be handled by Golbot
type Command interface {
	// GetRegex must return a regex that will be used to match against a message
	GetRegex() string
	// Do is the function getting called when Golbot receive a message
	// and the regex returned by GetRegex match a received message.
	// The KeepLooking type returned define if Golbot should keep
	// looking for a command matching its message, or stop right now.
	Do(*discordgo.MessageCreate, []string) error
	// GetHelp returns the help text of a command
	GetHelp() string
	// GetHelp returns the name of a command
	GetName() string
}

// AddCommands take a slice of structs implementing the Command interface
// and add it to the list of commands that will be used
// when Golbot receive a message
func (g *Golbot) AddCommands(cmds []Command) error {
	var err strings.Builder

	if g.commands == nil {
		g.commands = make(map[string]Command)
	}

	for _, cmd := range cmds {
		r := cmd.GetRegex()

		if r == "" {
			err.WriteString(fmt.Sprintf("Could not add %s command: Empty regex\n", cmd.GetName()))
			continue
		}
		g.commands[r] = cmd
	}

	if err.Len() == 0 {
		return nil
	}

	return errors.New(err.String())
}
