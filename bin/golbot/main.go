package main

import (
	"log"
	"os"

	"bitbucket.org/drannoc/golbot"
	"bitbucket.org/drannoc/golbot/pkg/config"
	"github.com/monkeydioude/golmods"
)

func main() {
	conf := config.HandleConfig()
	golbot, err := golbot.New(conf.GetBotToken())

	if err != nil {
		log.Fatal(err)
	}

	err = os.MkdirAll(conf.CachePath, 0766)
	if err != nil {
		log.Println(err)
	}

	golbot.AddCommands(golmods.GetCommands(conf.CachePath, golbot.GetDiscordSession()))
	golbot.CreateCacheDir()
	err = golbot.Run()
	if err != nil {
		log.Fatal(err)
	}
}
