package golbot

import (
	"testing"

	"github.com/bwmarrin/discordgo"
)

type dummy struct {
	regex       string
	help        string
	keepLooking KeepLooking
	do          func(*discordgo.Session, *discordgo.MessageCreate, []string) KeepLooking
	name        string
}

func (d *dummy) GetRegex() string {
	return d.regex
}

func (d *dummy) GetHelp() string {
	return d.help
}

func (d *dummy) Do(s *discordgo.Session, m *discordgo.MessageCreate, p []string) KeepLooking {
	return d.do(s, m, p)
}

func (d *dummy) GetName() string {
	return d.name
}

func TestIFailOnEmptyRegex(t *testing.T) {
	trial := []Command{&dummy{}}
	g := &Golbot{}

	if err := g.AddCommands(trial); err == nil {
		t.Error("AddCommands returned error should not be nil")
	}
}

func TestICanAddCommandOnRegexNotEmpty(t *testing.T) {
	trial := []Command{
		&dummy{
			regex: "/test",
		},
	}
	g := &Golbot{}

	if err := g.AddCommands(trial); err != nil || len(g.commands) == 0 {
		t.Error("AddCommands return ederror should be nil and have a new Command in cmds")
	}
}

func TestIDontTreatMsgOnEmptyContent(t *testing.T) {
	s := &discordgo.Session{}
	m := &discordgo.MessageCreate{
		Message: &discordgo.Message{},
	}

	dummy := []Command{&dummy{
		regex: "/test",
		help:  "wesh alors",
		do: func(s *discordgo.Session, m *discordgo.MessageCreate, p []string) KeepLooking {
			t.Error("Do() should not have been called")
			return false
		},
	}}
	g := &Golbot{}

	if err := g.AddCommands(dummy); err != nil {
		t.Error("Should have been able to Add a Command here")
	}
	g.messageHandler(s, m)
}

func TestIDontTreatMsgWhenAuthorIsBot(t *testing.T) {
	s := &discordgo.Session{}
	m := &discordgo.MessageCreate{
		Message: &discordgo.Message{
			Content: "/shlorp",
			Author: &discordgo.User{
				ID: botID,
			},
		},
	}

	dummy := []Command{&dummy{
		regex: "/shlorp",
		help:  "heyoo",
		do: func(s *discordgo.Session, m *discordgo.MessageCreate, p []string) KeepLooking {
			t.Error("Do() should not have been called")
			return false
		},
	}}
	g := &Golbot{}

	if err := g.AddCommands(dummy); err != nil {
		t.Error(err)
	}
	g.messageHandler(s, m)
}
