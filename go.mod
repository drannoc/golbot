module bitbucket.org/drannoc/golbot

require (
	github.com/bwmarrin/discordgo v0.18.0
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/monkeydioude/golmods v1.0.1-0.20191119103627-cbc7dd2a6acf
	github.com/monkeydioude/gophy v0.0.0-20191030145642-82254903d9cd
	github.com/monkeydioude/graw v0.0.0-20191119105208-d4f1d7f00ace // indirect
	github.com/monkeydioude/lgtR v0.0.0-20191119103405-998a304de28c // indirect
	github.com/monkeydioude/tools v0.0.0-20190104233136-9cf139acb9b4
	go.etcd.io/bbolt v1.3.3 // indirect
)

go 1.13
