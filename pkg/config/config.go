package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
)

type config struct {
	Token      string `json:"token"`
	Rights     int    `json:"rights"`
	CachePath  string `json:"cache_path"`
	authString string
}

func newFromConfig(path string) (*config, error) {
	var c config

	r, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(r, &c)
	return &c, err
}

func (c *config) GetBotToken() string {
	return fmt.Sprintf("Bot %s", c.Token)
}

func HandleConfig() (conf *config) {
	var c string
	var err error

	flag.StringVar(&c, "c", "", "using config file to determine bot's token & rights")
	flag.Parse()
	conf, err = newFromConfig(c)

	if err != nil {
		log.Fatalf("[ERR ] Could not retrieve config. Reason: %s\n", err)
	}

	return conf
}
