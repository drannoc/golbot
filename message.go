package golbot

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

// Message defines the communication interface for sending
// message to a discord session.
type Message interface {
	Send(*discordgo.Session)
}

// HelpMsg implements Message interface. It is used to
// display the help of each registered Command
type HelpMsg struct {
	cmds *map[string]Command
	m    *discordgo.MessageCreate
}

const (
	_helpCmdIntro = "Golbot learned to respond to these Commands :\n"
)

// Send implements Message interface
func (h *HelpMsg) Send(s *discordgo.Session) {
	var msg strings.Builder

	msg.WriteString(_helpCmdIntro)

	for _, cmd := range *h.cmds {
		msg.WriteString("\t- ")
		msg.WriteString(cmd.GetHelp())
		msg.WriteRune('\n')
	}

	s.ChannelMessageSend(h.m.ChannelID, msg.String())
}
